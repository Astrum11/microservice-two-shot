from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import Shoe, BinVO
# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
      "import_href",
      "bin_number",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "id",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
        "bin_number": BinVODetailEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, id=None):
    if request.method == "GET":
        if id is not None:
            shoes = Shoe.objects.filter(id=id)
        else:
            shoes = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoeListEncoder,
            )
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content['bin'])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):

    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count >0})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=id).update(**content)
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
