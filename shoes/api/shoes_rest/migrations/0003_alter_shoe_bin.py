# Generated by Django 4.0.3 on 2023-06-02 02:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_remove_shoe_shoe_bin_shoe_bin_alter_binvo_bin_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoe',
            name='bin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='shoes', to='shoes_rest.binvo'),
        ),
    ]
