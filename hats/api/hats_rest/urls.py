from django.urls import path
from .views import list_hats

urlpatterns = [
    path('hats/', list_hats, name='api_list_hats'),
    path(
        'hats/<int:location_vo_id>/',
        list_hats,
        name='list_hat'
    )
]
