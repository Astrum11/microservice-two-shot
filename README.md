# Wardrobify

Team:

* Michael Nguyen - Hat microservice
* Alvin Liang - Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

Looked at the required items for the shoes model.
Setup insomnia to create and see list of bins from the wardrobe.
Setup the get/post backend.
Setup poller to get bin VO's.
Setup insomnia to test get/post backends
Setup shoes list in react.
Create a form to create a shoe.
Added delete feature to delete shoes.

## Hats microservice
LocationVO: Represents a location within the wardrobe where our hats is stored.
Has attributes like import_href(unique id), closet_name(name of the closet), section_number(section in the closet) and a self_number(specific shelf within a section)

Hat: Represents a hat. Has attibutes named after characterists of the hat ie,
fabric, style_name, color, and the picture_url. Uses a foreign key with LocationVO to indicate
the specific location within the wardrobe where the hat is stored.

On the front-end, the react components within HatForm, HatList fetches the data avaliable
(ie. LocationVO objects) from the wardrobe microservice using the poller.
In the back-end, it associates Hat objects with a specific LocationVO object in the wardrobe.
