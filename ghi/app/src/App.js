import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react'
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import HatsList from './HatsList';
import ShoeForm from './ShoeForm';

function App () {

  const [ shoes, setShoes ] = useState([]);
  const [ hats, setHats] = useState([]);

  async function getShoes () {
    const url = 'http://localhost:8080/api/shoes/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json()
      setShoes(data.shoes)
    }
  }


  async function getHats() {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json()
      setHats(data.hats)
    }
  }

  useEffect(() => {
      getShoes();
      getHats();
  }, [])

    if (shoes === undefined || hats === undefined) {
      return null;
    }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="new" element={<ShoeForm getShoes={getShoes} />} />
            <Route index element={<ShoesList shoes={shoes} getShoes={getShoes}  />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatsList hats={hats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
