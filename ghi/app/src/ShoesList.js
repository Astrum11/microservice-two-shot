import React, { useState, useEffect } from 'react'

function ShoesList({ shoes, getShoes }) {


    const handleDelete = async (shoeId) => {
      const url = 'http://localhost:8080/api/shoes/' + shoeId
      const response = await fetch(url, {method: "DELETE"});
      if (response.ok) {
        getShoes()

      } else {
        console.log("Didn't delete")
      }
    }


    return(
        <table className="table table-dark table-striped">
        <thead>
          <tr>
            <th>Delete Shoe</th>
            <th>Manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Bin Number</th>
            <th>Picture url</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
            <tr key={shoe.id}>
              <td>
              <button onClick={() => handleDelete(shoe.id)} className="btn btn-warning">Delete</button>
              </td>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.bin.bin_number}</td>
              <td>{shoe.picture_url}</td>
            </tr>
            );
          })}
        </tbody>
      </table>
    )
}

export default ShoesList
