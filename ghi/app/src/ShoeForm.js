import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

function ShoeForm({getShoes}) {

    const navigate = useNavigate()
    const [bins, setBins] = useState([])
    const [manufacturer, setManufacturer] = useState('')
    const [name, setName] = useState('')
    const [color, setColor] = useState('')
    const [picture, setPicture] = useState('')
    const [bin, setBin] = useState('')


    useEffect(() => {
    async function fetchData () {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }
        fetchData();
    }, [])

    function handleManufacturerChange (e) {
        const value = e.target.value;
        setManufacturer(value)
    }

    function handleNameChange (e) {
        const value = e.target.value;
        setName(value)
    }

    function handleColorChange (e) {
        const value = e.target.value;
        setColor(value)
    }

    function handlePictureChange (e) {
        const value = e.target.value;
        setPicture(value)
    }

    function handleBinChange (e) {
        const value = e.target.value;
        setBin(value)
    }

    async function handleSubmit (e) {
        e.preventDefault()
        const data = {}

        data.manufacturer = manufacturer
        data.name = name
        data.color = color
        data.picture_url = picture
        data.bin = bin

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
        const newShoe = await response.json();
        getShoes()

        }
        e.target.reset()
        // navigate('/shoes')
    }

    return (

        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Shoe Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="name" required type="text" id="name" name="name" className="form-control" />
                <label htmlFor="name">Shoe Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="color" required type="text" id="color" name="color" className="form-control" />
                <label htmlFor="color">Shoe Color</label>
              </div>
              <div className="mb-3">
                <label htmlFor="picture_url">Picture Url</label>
                <input onChange={handlePictureChange} placeholder="Picture Url" type="url" id="picture_url" name="picture_url" className="form-control" />
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} name="Bin" id="Bin" className="form-select" required>
                  <option value="">Choose a Shoe Collection</option>
                  {bins.map(bin => {
                    return (
                        <option key={bin.href} value={bin.href}>
                            {bin.bin_number}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    )
}

export default ShoeForm
