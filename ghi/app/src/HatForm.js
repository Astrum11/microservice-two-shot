import React, { useState, useEffect } from 'react';

function HatForm() {
  const [closets, setClosets] = useState([]);
  const [fabric, setFabric] = useState();
  const [styleName, setStyleName] = useState();
  const [color, setColor] = useState();
  const [pictureUrl, setPictureUrl] = useState();
  const [closet, setCloset] = useState();

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setClosets(data.closets);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleFabricChange = (e) => {
    const value = e.target.value;
    setFabric(value);
  };

  const handleStyleNameChange = (e) => {
    const value = e.target.value;
    setStyleName(value);
  };

  const handleColorChange = (e) => {
    const value = e.target.value;
    setColor(value);
  };

  const handlePictureUrlChange = (e) => {
    const value = e.target.value;
    setPictureUrl(value);
  };

  const handleClosetChange = (e) => {
    const value = e.target.value;
    setCloset(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      fabric,
      style_name: styleName,
      color,
      picture_url: pictureUrl,
      closet,
    };

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);

      setFabric();
      setStyleName();
      setColor();
      setPictureUrl();
      setCloset();
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} placeholder="Fabric" required type="text" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStyleNameChange} placeholder="Style Name" required type="text" id="style_name" className="form-control" />
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} placeholder="Color" type="text" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureUrlChange} placeholder="Picture URL" required type="url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleClosetChange} id="closet" className="form-select" required>
                <option value="">Choose a hat closet</option>
                {closets.map(closet => {
                  return (
                    <option key={closet.href} value={closet.href}>
                      {closet.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
