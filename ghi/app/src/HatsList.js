function HatsList({ hats }) {
    return(
        <table className="table table-dark table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Picture url</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
            <tr key={hat.href}>
              <td>{hat.fabric}</td>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td>{hat.picture_url}</td>
            </tr>
            );
          })}
        </tbody>
      </table>
    )
}

export default HatsList
